package pageObjects;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "li[data-title=HOTELS]")
    private WebElement searchByHotelsBtn;

    @FindBy(css = "#s2id_autogen9")
    private WebElement cityField;

    @FindBy(css = "li.select2-results-dept-1")
    private List<WebElement> foundCitiesList;

    @FindBy(css = ".dpd1")
    private WebElement checkInDateField;

    @FindBy(css = ".dpd2")
    private WebElement checkOutDateField;

    @FindBy(css = "#travellersInput")
    private WebElement peopleAmountBtn;

    @FindBy(css = "#adultInput")
    private WebElement adultsAmountField;

    @FindBy(css = "#childInput")
    private WebElement childsAmountField;

    @FindBy(xpath = "//button[contains(text(),'Search')]")
    private WebElement startSearchingBtn;

    public HomePage openHotelsSearchTab() {
        searchByHotelsBtn.click();
        return this;
    }

    public HomePage fillDataForHotelsSearch(String city, String checkInDate, String checkOutDate, String adultsForSearch, String childForSearch) {
        inputCityForSearching(city);
        inputDateForSearching(checkInDate,checkOutDate);
        inputPeopleForSearching(adultsForSearch, childForSearch);
        return this;
    }

    public HomePage inputCityForSearching(String city){
        cityField.sendKeys(city);
        waitForListOfElementsVisible(foundCitiesList);
        for (WebElement element : foundCitiesList) {
            if (StringUtils.contains(element.getText(), city)) {
                element.click();
                break;
            }
        }
        return this;
    }

    public HomePage inputDateForSearching(String checkInDate, String checkOutDate){
        checkInDateField.sendKeys(checkInDate);
        checkOutDateField.sendKeys(checkOutDate);
        return this;
    }

    public HomePage inputPeopleForSearching(String adultsAmount, String childAmount) {
        peopleAmountBtn.click();
        waitForElementClickable(adultsAmountField);
        adultsAmountField.sendKeys(adultsAmount);
        childsAmountField.sendKeys(childAmount);
        return this;
    }

    public SearchResultsPage startSearchingForHotels() {
        clickAndWaitForNewPageToLoad(startSearchingBtn);
        return new SearchResultsPage(driver, wait);
    }

}

