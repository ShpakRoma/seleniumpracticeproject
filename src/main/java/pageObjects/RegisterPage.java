package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage extends BasePage {

    public RegisterPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver,this);
    }

    @FindBy(css ="#headersignupform input[name='firstname']")
    private WebElement firstNameField;

    @FindBy(css ="#headersignupform input[name='lastname']")
    private WebElement lastNameField;

    @FindBy(css ="#headersignupform input[name='phone']")
    private WebElement mobileNumberField;

    @FindBy(css ="#headersignupform input[name='email']")
    private WebElement emailField;

    @FindBy(css ="#headersignupform input[name='password']")
    private WebElement passwordField;

    @FindBy(css ="#headersignupform input[name='confirmpassword']")
    private WebElement confirmPasswordField;

    @FindBy(css ="#headersignupform .signupbtn")
    private WebElement signUpBtn;

    public RegisterPage fillRegistrationData(String firstName, String lastName, String mobileNumber, String email, String password) {
        firstNameField.sendKeys(firstName);
        lastNameField.sendKeys(lastName);
        mobileNumberField.sendKeys(mobileNumber);
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
        confirmPasswordField.sendKeys(password);
        return this;
    }

    public AccountPage signUp() {
        clickAndWaitForNewPageToLoad(signUpBtn);
        return new AccountPage(driver,wait);
    }

}
