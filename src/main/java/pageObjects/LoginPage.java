package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @FindBy(name = "username")
    private WebElement emailField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(css = ".loginbtn")
    private WebElement loginBtn;

    public AccountPage login(String email, String password) {
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
        clickAndWaitForNewPageToLoad(loginBtn);
        return new AccountPage(driver, wait);
    }

}
