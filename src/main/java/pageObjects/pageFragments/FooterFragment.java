package pageObjects.pageFragments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.BasePage;

public class FooterFragment extends BasePage {

    public FooterFragment(WebDriver driver, WebDriverWait wait){
        super(driver, wait);
        PageFactory.initElements(driver,this);
    }

    @FindBy(css ="#exampleInputEmail1" )
    private WebElement subscriptionEmailField;

    @FindBy(css ="#footer button.sub_newsletter" )
    private WebElement subscribeBtn;

    @FindBy(css =".subscriberesponse")
    private WebElement subscriptionResponse;

    public FooterFragment subscribeForNewsletter(String email) {
        subscriptionEmailField.sendKeys(email);
        subscribeBtn.click();
        return this;
    }

    public String getSubscriptionResponse() {
        waitForElementVisible(subscriptionResponse);
        return subscriptionResponse.getText();
    }

}
