package pageObjects.pageFragments;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.BasePage;
import pageObjects.LoginPage;
import pageObjects.RegisterPage;

import java.util.List;

@Getter
public class HeaderFragment extends BasePage {

    public HeaderFragment(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#collapse ul.navbar-side li")
    private WebElement accountBtn;

    @FindBy(css = "#collapse a[href*='login']")
    private WebElement loginBtn;

    @FindBy(css = "#collapse a[href*='register']")
    private WebElement signUpBtn;

    @FindBy(css = "#collapse a[href*='logout']")
    private WebElement logoutBtn;

    @FindBy(css = "#collapse ul.navbar-side > ul")
    private WebElement languageBtn;

    @FindBy(css = "#collapse ul.navbar-side > ul a")
    private List<WebElement> avaiableLanguages;

    public LoginPage goToLoginPage() {
        accountBtn.click();
        waitForElementClickable(loginBtn);
        clickAndWaitForNewPageToLoad(loginBtn);
        return new LoginPage(driver, wait);
    }

    public RegisterPage goToRegisterPage() {
        accountBtn.click();
        waitForElementClickable(loginBtn);
        clickAndWaitForNewPageToLoad(signUpBtn);
        return new RegisterPage(driver, wait);
    }

    public LoginPage logout() {
        accountBtn.click();
        clickAndWaitForNewPageToLoad(logoutBtn);
        return new LoginPage(driver, wait);
    }

    public HeaderFragment changePageLanguage(String language) {
        Actions builder = new Actions(driver);

        builder.moveToElement(languageBtn).build().perform();
        waitForListOfElementsVisible(avaiableLanguages);
        for (WebElement element : avaiableLanguages) {
            if ((element.getText()).equalsIgnoreCase(language)) {
                clickAndWaitForNewPageToLoad(element);
                break;
            }
        }
        return this;
    }

}
