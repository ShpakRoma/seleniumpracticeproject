package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.pageFragments.FooterFragment;
import pageObjects.pageFragments.HeaderFragment;
import helpers.DriverWaits;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public abstract class BasePage extends DriverWaits {

    protected WebDriver driver;
    private FooterFragment footerFragment;
    private HeaderFragment headerFragment;

    public BasePage(WebDriver driver, WebDriverWait wait) {
        super(wait);
        this.driver = driver;
    }

    public String getPageLanguage() {
        return getHeaderFragment().getLanguageBtn().getText();
    }

    public boolean isUserLogged() {
        WebElement element = getHeaderFragment().getAccountBtn();
        return !(element.getText()).equalsIgnoreCase("my account");
    }

    public Set<String> getDomainSpecificBrokenLinks(String domain) {
        Set<String> brokenLinks = new HashSet<>();

        List<WebElement> links = driver.findElements(By.cssSelector("a[href^='" + domain + "']"));
        for (WebElement element : links) {
            try {
                String url = element.getAttribute("href");
                HttpURLConnection connection = (HttpURLConnection) (new URL(url).openConnection());
                connection.setRequestMethod("HEAD");
                connection.connect();

                int respCode = connection.getResponseCode();
                if (respCode >= 400) {
                    brokenLinks.add(url);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return brokenLinks;
    }

    public FooterFragment getFooterFragment() {
        return (footerFragment == null) ? footerFragment = new FooterFragment(this.driver, this.wait) : footerFragment;
    }

    public HeaderFragment getHeaderFragment() {
        return (headerFragment == null) ? headerFragment = new HeaderFragment(this.driver, this.wait) : headerFragment;
    }

}
