package helpers;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public abstract class Utils {

    public static void takeScreenshot(WebDriver driver, String fileSavePath, String methodName) {
        try {
            File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileHandler.copy(srcFile, new File(fileSavePath + methodName + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //It's a "practice" project, thus below method is left as is and not removed (working but not used currently)
    public static Properties getObjectRepository(String pathToFile) {
        Properties prop = null;

        try {
            prop = new Properties();
            FileInputStream stream = new FileInputStream(new File(pathToFile));
            prop.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return prop;
    }

}
