package helpers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public abstract class DriverWaits {

    protected WebDriverWait wait;

    protected DriverWaits(WebDriverWait wait) {
        this.wait = wait;
    }

    protected void clickAndWaitForNewPageToLoad(WebElement element) {
        element.click();

        while (true) {
            try {
                element.getTagName();
            } catch (StaleElementReferenceException | NoSuchElementException e) {
                wait.until((ExpectedCondition<Boolean>) driver ->
                        ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
                break;
            }
        }
    }

    protected void waitForElementClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitForElementVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitForListOfElementsVisible(List<WebElement> elements) {
        wait.until(ExpectedConditions.visibilityOfAllElements(elements));
    }

}
