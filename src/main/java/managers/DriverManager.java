package managers;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

@Getter
public class DriverManager {

    private WebDriver driver;
    private WebDriverWait wait;

    public DriverManager(BrowserType browserType, long driverWaitTime) {
        createWebDriver(browserType);
        createWebDriverWait(driverWaitTime);
    }

    private void createWebDriver(BrowserType browserType) {
        switch (browserType) {
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\Files\\Drivers\\geckodriver.exe");
                driver = new FirefoxDriver();
                break;
            case CHROME:
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\files\\drivers\\chromedriver.exe");
                driver = new ChromeDriver();
                break;
        }
    }

    private void createWebDriverWait(long driverWaitTime) {
        wait = new WebDriverWait(driver, driverWaitTime);
    }

    public void shutdown() {
        driver.quit();
    }

    public DriverManager maximizeWindow() {
        driver.manage().window().maximize();
        return this;
    }

    public DriverManager loadUrl(String url) {
        driver.get(url);
        return this;
    }

}
