package managers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.*;

public class PageManager {

    private WebDriver driver;
    private WebDriverWait wait;

    public PageManager(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public <TPage extends BasePage> TPage getPage(Class<TPage> pageClass) {
        try {

            return pageClass.getDeclaredConstructor(WebDriver.class, WebDriverWait.class).newInstance(this.driver, this.wait);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
