package customWebElementsWorkInProgress.elements;

import customWebElementsWorkInProgress.factory.base.Element;
import customWebElementsWorkInProgress.factory.base.ImplementedBy;

@ImplementedBy(ButtonBase.class)
public interface Button extends Element {


    /*
    void PerformClick();
    String GetButtonText();
    void PerformSubmit();
    void Wait();
    void WaitForVisible();
    void Click();

    */
}
