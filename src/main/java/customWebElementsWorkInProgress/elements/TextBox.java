package customWebElementsWorkInProgress.elements;

import customWebElementsWorkInProgress.factory.base.Element;
import customWebElementsWorkInProgress.factory.base.ImplementedBy;

@ImplementedBy(TextBoxBase.class)
public interface TextBox extends Element {

}
