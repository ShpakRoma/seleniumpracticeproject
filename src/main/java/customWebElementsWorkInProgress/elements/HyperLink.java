package customWebElementsWorkInProgress.elements;

import customWebElementsWorkInProgress.factory.base.Element;
import customWebElementsWorkInProgress.factory.base.ImplementedBy;

@ImplementedBy(HyperLinkBase.class)
public interface HyperLink extends Element {

    /*
    void clickLink();

    String getUrlText();

    boolean checkUrlTextContains(String containsText);

    */
}
