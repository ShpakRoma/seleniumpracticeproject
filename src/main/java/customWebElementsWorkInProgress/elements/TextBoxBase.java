package customWebElementsWorkInProgress.elements;

import customWebElementsWorkInProgress.factory.base.ElementBase;
import org.openqa.selenium.WebElement;

public class TextBoxBase extends ElementBase implements TextBox   {

    public TextBoxBase(WebElement element) {
        super(element);
    }

}
