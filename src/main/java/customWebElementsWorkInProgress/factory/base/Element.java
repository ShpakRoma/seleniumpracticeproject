package customWebElementsWorkInProgress.factory.base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.internal.WrapsElement;

@ImplementedBy(ElementBase.class)
public interface Element extends WebElement, WrapsElement, Locatable {

/*
    Element waitForElementClickable(Element element);

    Element waitForListOfElementsVisible(List<Element> elements);

    Element scrollToElement(Element element);

    Element waitWhileElementHasAttributeValue(Element element, String attribute, String value);

    Element clickAndWaitForNewPageToLoad(Element element);
*/
}
