package testRunConfig.listeners;

import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.testng.*;
import tests.BaseTest;

import static helpers.Utils.takeScreenshot;

public class TestListener extends BaseTest implements ITestListener {

    private static String screenshotsSavePath = System.getProperty("user.dir") + "\\Files\\Screenshots\\";

    @Override
    public void onStart(ITestContext context) {
    }

    @Override
    public void onFinish(ITestContext context) {
        extentReport.flush();
    }

    @Override
    public void onTestStart(ITestResult result) {
        ExtentTest extentTest = extentReport.createTest(result.getMethod().getMethodName(), result.getMethod().getDescription());
        test.set(extentTest);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        test.get().pass("Test passed");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Object failedTest = result.getInstance();
        WebDriver driver = ((BaseTest) failedTest).getDriverManager().getDriver();
        String methodName = result.getMethod().getMethodName();
        takeScreenshot(driver, screenshotsSavePath, methodName);

        test.get().fail(result.getThrowable());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        test.get().skip(result.getThrowable());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        test.get().fail(result.getMethod().getMethodName() + "failed but within success percentage");
    }

}
