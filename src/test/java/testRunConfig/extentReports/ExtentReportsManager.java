package testRunConfig.extentReports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReportsManager {

    private static ExtentReports extentReports;
    private static String reportFileName = "ExtentReport.html";
    private static String reportFolder = System.getProperty("user.dir") + "\\Files\\TestReports";
    private static String reportFileLoc = reportFolder + "\\" + reportFileName;

    public static ExtentReports getInstance() {
        if (extentReports == null)
            createInstance();
        return extentReports;
    }

    public static ExtentReports createInstance() {
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(reportFileLoc);

        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setTheme(Theme.STANDARD);
        htmlReporter.config().setDocumentTitle(reportFileLoc);
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName(reportFileLoc);

        extentReports = new ExtentReports();
        extentReports.attachReporter(htmlReporter);

        return extentReports;
    }

}
