package tests;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import managers.BrowserType;
import managers.DriverManager;
import managers.PageManager;
import lombok.Getter;
import org.testng.annotations.*;
import testRunConfig.extentReports.ExtentReportsManager;

@Getter
public class BaseTest {

    protected static ExtentReports extentReport = ExtentReportsManager.getInstance();
    protected static ThreadLocal<ExtentTest> test = new ThreadLocal<>();
    protected static String baseUrl = "https://www.phptravels.net/en";
    protected PageManager pageManager;
    protected DriverManager driverManager;
    protected int driverWaitTime = 15;

    @BeforeClass
    public void setUp() {
        driverManager = new DriverManager(BrowserType.FIREFOX, driverWaitTime).maximizeWindow();
        pageManager = new PageManager(driverManager.getDriver(), driverManager.getWait());
    }

    @BeforeMethod
    public void loadBaseUrl() {
        driverManager.loadUrl(baseUrl);
    }

    @AfterClass
    public void tearDown() {
        driverManager.shutdown();
    }

}
