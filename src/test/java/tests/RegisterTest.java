package tests;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.HomePage;

import static org.assertj.core.api.Assertions.assertThat;

public class RegisterTest extends BaseTest {

    private HomePage homePage;

    @BeforeMethod
    public void pageObjectsCreation(){
        homePage = pageManager.getPage(HomePage.class);
    }

    //don't worry that we will spam our testing page with non existing mails, etc. Database is droped every 5 min
    @Test(description = "Should successfully register new user")
    public void newUserRegistrationTest() {
        String randomString = RandomStringUtils.randomAlphanumeric(8);
        String randomMobileNumber = RandomStringUtils.randomNumeric(9);

        Boolean newUserLogged = homePage
                .getHeaderFragment()
                .goToRegisterPage()
                .fillRegistrationData(randomString, randomString, randomMobileNumber, randomString + "@gmail.com", randomString)
                .signUp().isUserLogged();

        assertThat(newUserLogged).isEqualTo(true);
    }

}
