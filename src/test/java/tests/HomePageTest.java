package tests;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.HomePage;

import static org.assertj.core.api.Assertions.assertThat;

public class HomePageTest extends BaseTest {

    private HomePage homePage;

    @BeforeMethod
    public void pageObjectsCreation(){
        homePage = pageManager.getPage(HomePage.class);
    }

    @Test(description = "Should find at least one hotel as a search result")
    public void findHotelsWithSearchTest() {
        int foundHotelsCount = homePage
                .openHotelsSearchTab()
                .fillDataForHotelsSearch("Dubai", "23/08/2018", "27/08/2018", "4", "2")
                .startSearchingForHotels()
                .getSearchResults()
                .size();

        assertThat(foundHotelsCount).isGreaterThan(0);
    }

    @Test(description = "Should pass if no broken links on home page related to our domain")
    public void homePageOwnDomainBrokenLinksTest() {
        int brokenLinksCount = homePage
                .getDomainSpecificBrokenLinks(baseUrl)
                .size();

        assertThat(brokenLinksCount).isEqualTo(0);
    }

    @Test(description = "Should successfully subscribe for newsletter")
    public void subscribingForNewsletterTest() {
        String randomString = RandomStringUtils.randomAlphanumeric(8);

        String subscriptionResponse = homePage
                .getFooterFragment()
                .subscribeForNewsletter(randomString + "@mail.wow")
                .getSubscriptionResponse();

        assertThat(subscriptionResponse).isEqualTo("SUBSCRIBED SUCCESSFULLY");
    }

    @Test(description = "Should change page language to french")
    public void changePageLanguageToFrenchTest() {
        String currentPageLanguage = homePage
                .getHeaderFragment()
                .changePageLanguage("French")
                .getPageLanguage();

        assertThat(currentPageLanguage).isEqualToIgnoringCase("french");
    }

}
