package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.HomePage;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginTest extends BaseTest {

    private HomePage homePage;

    @BeforeMethod
    public void pageObjectsCreation(){
        homePage = pageManager.getPage(HomePage.class);
    }

    @Test(description = "Should successfully login with existing user credentials")
    public void loginWithExistingCredentialsTest() {
        Boolean logged = homePage
                .getHeaderFragment()
                .goToLoginPage()
                .login("user@phptravels.com", "demouser").isUserLogged();

        assertThat(logged).isTrue();
    }

    @Test(description = "Logged user should successfully logout", dependsOnMethods = "loginWithExistingCredentialsTest")
    public void logoutTest() {
        Boolean logged = homePage
                .getHeaderFragment()
                .logout()
                .isUserLogged();

        assertThat(logged).isFalse();
    }

}
